import React, { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import styles from '../styles/Home.module.css'
import Select from 'react-select'
import swal from 'sweetalert';
import NumericInput from 'react-numeric-input';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import * as moment from 'moment'
import Alert from 'react-bootstrap/Alert'


const uri = "https://1iixcnonpe.execute-api.us-east-1.amazonaws.com/Prod"

export default function Home() {
    const [isLoading, setIsLoading] = useState(true);
    const [ errors, setErrors ] = useState(false)
    const [errorsMessage, setErrorsMessage] = useState('');
    const [orders, setOrders] = useState([]);
    const [order, setOrder] = useState([]);
    const [selectProductsCustomer, setSelectProductsCustomer] = useState([]);
    const [productsCustomer, setProductsCustomer] = useState([]);
    const [total, setTotal] = useState(0);
    const [products, setProducts] = useState([]);
    const [customer, setCustomer] = useState(null);
    const [customers, setCustomers] = useState([]);
    const [currentCustomer, setCurrentCustomers] = useState('');
    const [modalOrder, setModalOrder] = useState(false);


    useEffect(() => {

        getOrders()
        getCustomers()


    }, [products]); //

    function addProduct() {
        if(order.customer !== currentCustomer){
            let newProducts = [...products];
            setProducts(newProducts)
            setCurrentCustomers(order.customer)
        }

        if((products.length < selectProductsCustomer.length)||(products.length === 5)){
            setProducts([...products, {
                'quantity': 1,
                'description': '',
                'price': '',
                'total': '',
                'id': ''
            }])
        }
    }


    function sendDataForm(){
        fetch(uri + '/orders',
            {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({'order': order, 'products': products}),
            })
            .then(res => {
                getOrders()
                setModalOrder(false);
                swal("Order Saved!", "Your Order saved correctly!", "success")
            })
            .then(json => console.log(json))
    }

    const submit = async e => {
        e.preventDefault()
        //validations
        let error_duplicate_products = false
        let error_product_empty = false
        let error_max_quantity = false


        //check empty
        const product = products.find( product => product.id === "");
        error_product_empty = (product !== undefined)
        setErrors(error_product_empty)
        setErrorsMessage('Some products are empty')

        //check duplicates
        if(!error_product_empty){
            error_duplicate_products = (new Set(products.map(p => p.id))).size !== products.length
            setErrors(error_duplicate_products)
            setErrorsMessage('You have duplicate products')
        }

        if(!error_duplicate_products){
            const product = products.find( product => product.quantity > 5);
            error_max_quantity = (product !== undefined)
            setErrors(error_max_quantity)
            setErrorsMessage('Maximum quantity per product is 5')
        }
      
        //if have errors not submmit
        if((error_product_empty) || (error_duplicate_products) || (error_max_quantity)){
            return;
        }else{
            sendDataForm()
        }

    }

    async function getOrders() {
        await fetch(uri + "/orders")
            .then(res => res.json())
            .then(
                (result) => {
                    setOrders(result.data)
                },
                (error) => {
                    console.log(error)
                }
            )
    }


    async function getCustomers() {
        await fetch(uri + "/customers")
            .then(res => res.json())
            .then(
                (result) => {
                    let data = []
                    result.data.map((customer, index) => {
                        data.push({value: customer.id, label: customer.name})
                    })

                    setCustomers(data)
                    setIsLoading(false)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    async function filterOrderbyCustomer(id) {
        setIsLoading(true)
        await fetch(uri + "/orders?customer=" + id)
            .then(res => res.json())
            .then(
                (result) => {
                    setOrders(result.data)
                    setIsLoading(false)
                    setCustomer(id)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    async function productsByCustomer(id) {
        await fetch(uri + "/customers/"+ id)
            .then(res => res.json())
            .then(
                (result) => {

                    let data = []
                    result.data.products.map((product, index) => {
                        data.push({value: product.id, label: product.name})
                    })
                    setSelectProductsCustomer(data)
                    setProductsCustomer(result.data.products)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    function sumTotal(productList){
        let sum = 0
        productList.map((product, index) => {
            sum += (product.price * product.quantity)
        })
        setTotal(sum)
    }

    function getProduct(id, index){
        let newProducts = [...products];
        const product = productsCustomer.find( product => product.id === id);

        products.map((item, i) => {
            if (index === i) {
                newProducts[index].quantity = 1;
                newProducts[index].description = product.description;
                newProducts[index].price = product.price;
                newProducts[index].id = product.id;
                newProducts[index].total = product.price;
            }
        });

        setProducts(newProducts)
        sumTotal(newProducts)
    }

    function setQuantityProduct(quantity, index){
        let newProducts = [...products];

        products.map((item, i) => {
            if (index === i) {
                newProducts[index].quantity = quantity;
            }
        });

        setProducts(newProducts)
        sumTotal(newProducts)

    }

    function deleteProduct(id){
        let newProducts = [...products];
        let products_list = newProducts.filter( product => product.id !== id);
        setProducts(products_list)
        sumTotal(products_list)

    }

    async function filterOrderbyDate(event, picker) {

        let start_date = moment(picker.startDate).format('YYYY-MM-DD')
        let end_date = moment(picker.endDate).format('YYYY-MM-DD')
        let filter_date = "start_date=" + start_date + '&end_date='+ end_date
        let filter_customer = customer != null  ? "&customer=" + customer : ''

        await fetch(uri + "/orders?" + filter_date + filter_customer)
            .then(res => res.json())
            .then(
                (result) => {
                    setOrders(result.data)
                    setIsLoading(false)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    function renderTableData(){
        return orders.map((order, index) => {
            const {id, creation_date, delivery_address, price, detail} = order //destructuring
            return (
                <tr key={id}>
                    <td>{creation_date}</td>
                    <td>{id}</td>
                    <td>$ {price}</td>
                    <td>{delivery_address}</td>
                    <td>{ detail.map((product, index) => product.quantity + ' x ' + product.product.name + ' ') }</td>
                </tr>
            )
        })
    }


    return (
        <div className={styles.container}>
            <Head>
                <title>Beitech Test</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Modal size="lg" show={modalOrder} onHide={() => setModalOrder(false)} aria-labelledby="example-modal-sizes-title-lg">
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        Create New Order
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form  onSubmit={submit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Customer</Form.Label>
                            <Select
                                required={true}
                                options={customers}
                                name="order[customer]"
                                onChange={(e) => { setOrder({ ...order, customer: e.value }) ; productsByCustomer(e.value)}}
                                placeholder ={"Select a Customer..."}
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Delivery Address</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                name="order[delivery_address]"
                                value={order.delivery_address}
                                placeholder="Example: 12 Street, Valledupar"
                                onChange={e => setOrder({ ...order, delivery_address: e.target.value })}
                            />
                            <Form.Control.Feedback type="invalid">
                                Please choose a username.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Row>
                            <Col xs={12} md={8}>
                                <h4>Products List</h4>
                            </Col>
                            <Col xs={12} md={4} className="text-right">
                                <Button variant="primary" disabled={order.customer === undefined || products.length >= selectProductsCustomer.length} onClick={() => addProduct()}>
                                    Add Product
                                </Button>
                            </Col>
                        </Row>
                        <hr/>
                        <Row>
                            <Col xs={12} >
                                <Table striped bordered hover>
                                    <thead>
                                    <tr>
                                        <th className="col-3">Product</th>
                                        <th className="col-3">Description</th>
                                        <th className="col-3">Price</th>
                                        <th className="col-1">Quantity</th>
                                        <th className="col-1">Total</th>
                                        <th className="col-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { products.map((product, index) => (
                                        <tr>
                                            <td>
                                                <Select options={selectProductsCustomer} onChange={e =>  getProduct(e.value, index)} placeholder ={"Select a Product..."}/>
                                            </td>
                                            <td>
                                                {product.description}
                                            </td>
                                            <td>
                                                $ {product.price}
                                            </td>
                                            <td>
                                                <NumericInput  className="form-control" disabled={product.price === ""} min={1} max={5} onChange={(e) => setQuantityProduct(e, index)}  value={product.quantity}/>
                                            </td>
                                            <td>
                                                {product.price * product.quantity}
                                            </td>
                                            <td>
                                                <Button variant="primary"  onClick={() => deleteProduct(product.id)}>
                                                    Delete
                                                </Button>
                                            </td>
                                        </tr>
                                    ))

                                    }
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colSpan="4"><strong>Total</strong></td>
                                        <td colSpan="2" className="text-right"><strong>$ {total}</strong></td>
                                    </tr>
                                    </tfoot>
                                </Table>
                            </Col>
                        </Row>
                        <Row>
                            { errors &&
                            <Col xs={12} >
                                <Alert variant={'danger'}>
                                    ERROR — {errorsMessage}
                                </Alert>
                            </Col>
                            }
                            <Col xs={12} className="text-right">
                                <Button disabled={products.length === 0} variant="primary" type="submit">
                                    Send
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>

            <Container>
                <Jumbotron>
                    <h1>Beitech Test</h1>
                    <p>
                        This is a test to save Products implementing Api Rest with Flask using AWS Lambda, React and Postgres Database.
                    </p>
                </Jumbotron>
                <Row>
                    <Col xs={12} >
                        <h1>Orders List</h1>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={4}>
                        <Select options={customers} onChange={(value) => filterOrderbyCustomer(value.value)} placeholder ={"Select a Customer..."}/>
                    </Col>
                    <Col xs={12} md={4}>
                        <DateRangePicker onApply={filterOrderbyDate}>
                            <input type="text" className="form-control" />
                        </DateRangePicker>
                    </Col>
                    <Col xs={12} md={4} className="text-right">
                        <Button variant="primary" onClick={() => setModalOrder(true)}>
                            Create Order
                        </Button>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col xs={12} >
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Creation Date</th>
                                <th>Order ID</th>
                                <th>Total</th>
                                <th>Delivery Address</th>
                                <th>Products</th>
                            </tr>
                            </thead>
                            <tbody>
                            { isLoading ?
                                <tr className="text-center">
                                    <td colSpan="5">Loading data ...</td>
                                </tr>
                                :
                                renderTableData()
                            }
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>

            <footer className={styles.footer}>
                <a
                    href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Powered by{' '}
                    <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
                </a>
            </footer>
        </div>
    )
}
